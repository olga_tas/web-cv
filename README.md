<!DOCTYPE html>
<html lang="en-US" dir="ltr">
  <head>
    <meta charset="UTF-8" />
    <title>Web CV - Olga Tas</title>
    <link type="image/x-icon" href="images/favicon.ico" />
  </head>
  <body>
    <!-- This page is created for the 'Introduction to HTML' training course -->
    <header>
      <h1>Web CV - Olga Tas</h1>
    </header>
    <aside>
      <h2>Phone number</h2>
      <p>+905005005050</p>
    </aside>
    <main>
      <section>
        <h2>Summary</h2>
        <p>
          I'm an enthusiastic and detail-oriented Junior Frontend Developer.
        </p>
        <p>
          I'm seeking an entry-level position with Company to use my skills in
          coding, troubleshooting complex problems, and assisting in the timely
          completion of projects.
        </p>
      </section>
      <section>
        <h2>Education</h2>
        <p>Belarusian State University</p>
        <p>Managament - Bachelor's degree, 2012</p>
      </section>
      <section>
        <h2>Work Experience</h2>
        <p>
          R Group International Cargo Trucking - Customer Service Manager,
          2014-2018
        </p>
      </section>
    </main>
    <footer>© My Copyright</footer>
  </body>
</html>
